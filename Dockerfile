FROM python:3.12.4-bookworm

RUN apt-get update && \
    apt-get install -y python3-setuptools nodejs npm

COPY entrypoint /entrypoint

RUN chmod +x /entrypoint/init.py

RUN  pip3 install --upgrade pip \
  && pip3 install -r /entrypoint/requirements.txt

RUN adduser jupyter --home /home/jupyter --disabled-password --quiet

#USER jupyter

WORKDIR /home/jupyter

ENTRYPOINT ["/entrypoint/init.py"]
