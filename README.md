# Jupyter

Curated tools to support meta-analysis, thematic analysis, and systematic reviews.

## Usage

```bash
docker run -it --rm -p 8888:8888 \
  -v ${PWD}:/home/jupyter \
  -d jupyter lab --no-browser --disable-notebook-app-token --allow-root
```
