# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
[markdownlint](https://dlaa.me/markdownlint/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.4.0] - 2023-04-12

### Added to v0.4.0

- Added disable warnings logic

## [v0.3.0] - 2023-04-10

### Added to v0.3.0

- Added modin-spreadsheet

## [v0.2.0] - 2023-04-09

### Added to v0.2.0

- Added Modin.pandas

## [v0.1.0] - 2023-04-01

### Added to v0.1.0

- Initial release
